from typing import List


class BotCommand(object):
    name: str
    description: str = ''
    detailed_description: str
    is_hidden: bool = False

    def __init__(self):
        self.__keys = []
        self.description = ''
        command_list.append(self)

    @property
    def keys(self):
        return self.__keys

    @keys.setter
    def keys(self, lst):
        for x in lst:
            self.__keys.append(x.lower())

    def process(self):
        pass


command_list: List[BotCommand] = []
