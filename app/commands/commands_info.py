from app.command_handler import command_list, BotCommand
from fuzzywuzzy import fuzz
from typing import Dict, Tuple, Union


def get_commands_info(data: Dict, *args, **kwargs) -> Tuple[str, str]:
    user_message = data['text'].split()
    msg = attachment = ''

    if len(user_message) == 1:
        msg = 'Список всех доступных команд:\n'
        for c in command_list:
            if not c.is_hidden:
                msg += f'{c.keys[0]} - {c.description}\n'

        return msg, attachment
    elif len(user_message) >= 2:
        cmd_name = user_message[1:]

        for c in command_list:
            if fuzz.partial_token_sort_ratio(c.name, cmd_name) >= 85:
                msg = '''
                Информация о команде "{}":
                
                {}
                
                Ключевые слова, которыми можно вызвать команду:
                    {}
                
                '''.format(c.name, c.description, ', '.join(c.keys))

                return msg, attachment


commands_info = BotCommand()
commands_info.name = 'помощь'
commands_info.keys = ['помощь', 'инфо', 'информация', 'info', 'help']
commands_info.description = 'отображает список всех доступных команд бота, показывает информацию о каждой отдельной команде'
commands_info.process = get_commands_info
