from app import vk
from app.command_handler import command_list, BotCommand
from config import PROJECTS_INFO_PATH
from fuzzywuzzy import process, fuzz
import json
from typing import Dict, Tuple


def get_attachment(data: Dict) -> Tuple[str, str]:
    projects_ids = {}
    msg, attachment = '', ''

    try:
        with open(PROJECTS_INFO_PATH, 'r', encoding='utf-8') as json_file:
            projects_ids = json.load(json_file)
    except Exception as err_msg:
        print(err_msg)
        return 'Возникла ошибка, список проектов временно недоступен', ''

    user_message = data['text'].split()

    if len(user_message) == 1:
        msg = 'Для студентов первого курса доступны следующие проекты:\n'

        for line in projects_ids.keys():
            msg += '- ' + line + '\n'

        msg += 'Дополнительную информацию о проектах и наставниках вы можете найти на proictis.sfedu.ru'

        return msg, ''
    elif len(user_message) >= 2:
        project_name = process.extractOne(
            query=' '.join(user_message[1:]),
            choices=projects_ids.keys(),
            scorer=fuzz.partial_token_sort_ratio
        )[0]

        msg = f'Информация о проекте "{project_name}"'
        attachment = projects_ids.get(project_name, '')

        return msg, attachment


project_list = BotCommand()
project_list.name = 'проекты'
project_list.keys = ['проекты']
project_list.description = 'отображает все проекты, доступные студентам первого курса на данный момент'
project_list.process = get_attachment
