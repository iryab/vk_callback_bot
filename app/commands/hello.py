from app import command_handler
from app import vk


def say_hello(data, *args, **kwargs):
    user_first_name = vk.api_bot.users.get(
        user_ids=data['from_id'],
        fields='first_name'
    )[0]['first_name']

    greeting = 'Привет, ' + user_first_name + '!'

    return greeting, ''


hello_to_user_command = command_handler.BotCommand()
hello_to_user_command.name = 'привет'
hello_to_user_command.keys = ['привет', 'прив', 'добрый день', 'здраствуй', 'хай', 'hello']
hello_to_user_command.process = say_hello
hello_to_user_command.description = 'тестовая функция для проверки методов API, отправляет пользователю приветствие с его/ее именем'
