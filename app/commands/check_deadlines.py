import datetime
# import pytz
import time
import schedule
from app import command_handler

DEADLINES = {
    'Отбор тем творческих проектов:': datetime.date(
        day=1, month=9,
        year=datetime.date.today().year - 1 if
        datetime.date.today().month < 9 else
        datetime.date.today().year
    ),
    'Размещение наставниками технических заданий в системе электронного обучения ИКТИБ:': datetime.date(
        day=1, month=12,
        year=datetime.date.today().year - 1
        if datetime.date.today().month < 9
        else datetime.date.today().year
    ),
    'Выбор студентаим темы творческого проекта и формирование проектных команд:': datetime.date(
        day=1, month=1,
        year=datetime.date.today().year + 1 if datetime.date.today().month >= 9
        else datetime.date.today().year
    ),
    'Выбор капитана команды, распределение ролей в команде,\
     составление плана выполнения проекта:': datetime.date(
        day=1, month=3,
        year=datetime.date.today().year + 1
        if datetime.date.today().month >= 9
        else datetime.date.today().year
    ),
    'Первый промежуточный отчет:': datetime.date(
        day=15, month=4,
        year=datetime.date.today().year + 1 if datetime.date.today().month >= 9
        else datetime.date.today().year
    ),
    'Второй промежуточный отчет:': datetime.date(
        day=1, month=6,
        year=datetime.date.today().year + 1 if datetime.date.today().month >= 9
        else datetime.date.today().year
    ),
    'Третий промежуточный отчет:': datetime.date(
        day=1, month=10,
        year=datetime.date.today().year + 1 if datetime.date.today().month >= 9
        else datetime.date.today().year
    ),
    'Индивидуальная оценка членов команды:': datetime.date(
        day=15, month=10,
        year=datetime.date.today().year + 1 if datetime.date.today().month >= 9
        else datetime.date.today().year
    ),
    'Загрузка капитаном презентации проекта. \
    Выставление наставником баллов за пояснительную записку:': datetime.date(
        day=20, month=10,
        year=datetime.date.today().year + 1 if datetime.date.today().month >= 9
        else datetime.date.today().year
    ),
    'Защита проекта:': datetime.date(
        day=5, month=11,
        year=datetime.date.today().year + 1 if datetime.date.today().month >= 9
        else datetime.date.today().year
    ),
}


def check_deadline(*args, **kwargs):
    today = datetime.date.today()

    for deadline in DEADLINES.keys():
        if str(datetime.date(day=1, month=6, year=2020)) == str(DEADLINES[deadline]):
            return 'Сегодня {0}:\nБлижайший дедлайн: {1}'.format(str(today), str(deadline).replace(':', '')), ''


def get_all_deadlines(*args, **kwargs):
    all_deadlines = 'Все дедлайны на этот учебный год:'
    for deadline in DEADLINES.keys():
        all_deadlines = all_deadlines + '\n\n' + \
                        str(deadline) + '\n' + str(DEADLINES[deadline])

    return all_deadlines, ''


# if __name__ == '__main__':
#     schedule.every().day.at('00:01').do(check_deadline)
#     while True:
#         schedule.run_pending()
#         time.sleep(1)

all_deadlines = command_handler.BotCommand()
all_deadlines.name = 'даты дедлайнов всех отчетов'
all_deadlines.keys = [
    'все дедлайны', 'будущие дедлайны', 'предстоящие дедлайны'
]
all_deadlines.description = 'показывает сроки сдачи отчетов на грядущий учебный год'
all_deadlines.process = get_all_deadlines

nearest_deadline = command_handler.BotCommand()
nearest_deadline.name = 'ближайший дедлайн'
nearest_deadline.keys = [
    'ближайший дедлайн', 'какой впереди дедлайн'
]
nearest_deadline.description = 'показывает ближайший дедлайн'
nearest_deadline.process = check_deadline
