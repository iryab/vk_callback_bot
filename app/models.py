from app import db


class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    vk_user_id = db.Column(db.Integer, index=True, nullable=True)
    # team_id = db.Column(db.Integer, db.ForeignKey('team.id'), nullable=True)
    is_tutor = db.Column(db.Boolean, default=False)
    is_admin = db.Column(db.Boolean, default=False)
    notification_subscribed = db.Column(db.Boolean, default=False)
    
    def __repr__(self):
        return f'ID: <{self.id}>, VK_UID: <{self.vk_user_id}>, NotificationSubscribed: <{self.notification_subscribed}>'

    def __init__(self, vk_user_id='', is_tutor=False, is_admin=False, notification_subscribed=False):
        self.vk_user_id = vk_user_id
        self.is_tutor = is_tutor
        self.is_admin = is_admin
        self.notification_subscribed = notification_subscribed
        

class Team(db.Model):
    __tablename__ = 'team'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, index=True)

    members = db.relationship(
        'User',
        backref='team',
        lazy='dynamic'
    )

    # tutor = db.relationship('Tutor', backref='team', uselist=False)
    tutor_id = db.Column(db.Integer, db.ForeignKey('tutor.id'))


class Tutor(db.Model):
    __tablename__ = 'tutor'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), index=True)
    team_id = db.Column(db.Integer, db.ForeignKey('team.id'), index=True)
    email = db.Column(db.Text)

    def __repr__(self):
        return f'TUTOR - UID: <{self.user_id}>, TEAM_ID: <{self.team_id}>'
