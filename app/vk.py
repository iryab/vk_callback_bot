import vk_api
from vk_api.utils import get_random_id
from config import VK_GROUP_TOKEN, VK_LOGIN, VK_PASSWORD


def auth_handler():
    key = input('Введите код аутентификации: ')
    remember_me = True
    return key, remember_me


bot_session = vk_api.VkApi(token=VK_GROUP_TOKEN, api_version='5.90')
api_bot = bot_session.get_api()

user_session = vk_api.VkApi(login=VK_LOGIN, password=VK_PASSWORD)
api_user = user_session.get_api()

try:
    user_session.auth(token_only=True)
except vk_api.AuthError as err_msg:
    print(err_msg)


def send_msg(peer_id: int, msg: str, attachments='', keyboard=None):
    api_bot.messages.send(
        peer_id=peer_id,
        message=msg,
        attachment=attachments,
        random_id=get_random_id(),
        keyboard=keyboard
    )
