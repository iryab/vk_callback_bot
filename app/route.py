from flask import request, jsonify
from app import app, db
from app.message_handler import create_answer


@app.route('/api/', methods=['POST'])
def processing():
    data = request.get_json(force=True, silent=True)
    if not data or 'type' not in data:
        return 'not ok'
    elif data['type'] == 'confirmation':
        return 'f1a37884'
    elif data['type'] == 'message_new':
        create_answer(data['object'])
        return 'ok'

    return 'ok'


@app.cli.command('initdb')
def init_db():
    db.create_all()


@app.cli.command('resetdb')
def reset_db():
    db.drop_all()
