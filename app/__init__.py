import sys
from os.path import dirname, join
from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

# not adding curr. dir to path causes import errors, trying to figure out how to fix it

sys.path.append(dirname(__file__))
sys.path.append(join(dirname(__file__), 'app'))
sys.path.append(join(dirname(__file__), join('app', 'commands')))

from config import Config
from app import vk

app = Flask(__name__)
app.config.from_object(Config)

db = SQLAlchemy(app)
migrate = Migrate(app, db)

from app import models
