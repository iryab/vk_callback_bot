from app import vk
from os import listdir, path
import importlib
from app.command_handler import command_list
from typing import Dict
from config import basedir
from fuzzywuzzy import fuzz, process


def load_modules():
    files = listdir(path=path.join(basedir, path.join('app', 'commands')))
    modules = filter(lambda x: x.endswith('.py'), files)
    for m in modules:
        importlib.import_module("commands." + m[0:-3])


def get_answer(data: Dict):
    message, attachment = '', ''

    keywords = [k for lst in [c.keys for c in command_list] for k in lst]

    best_match = process.extractOne(
        query=data['text'].lower(),
        choices=keywords,
        scorer=fuzz.partial_ratio
    )

    print('[BOT LOG] USER MESSAGE - {} :  BEST MATCH - {}'.format(
        data['text'].lower(),
        best_match
    ))

    if best_match[1] >= 80:
        cmnd_key = best_match[0]
    else:
        message = f'''
        Извините, я вас не понял.
        Может вы имели ввиду команду "{best_match[0]}"?
        Попробуйте написать "помощь", чтобы увидеть все команды бота.'
        '''

        return message, attachment

    for command in command_list:
        if cmnd_key in command.keys and not command.is_hidden:
            message, attachment = command.process(data)

    return message, attachment


def create_answer(data):
    load_modules()
    # if messages is from a user, peer_id will be equal to the same number as user_id
    # if message is from a group_chat, peer_id should be 2000000000 + chat_id
    peer_id = data['peer_id']
    message, attachment = get_answer(data)
    try:
        vk.send_msg(peer_id=peer_id, msg=message, attachments=attachment)
    except Exception as e:
        print(e)
