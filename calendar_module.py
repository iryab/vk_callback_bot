import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from fuzzywuzzy import fuzz
import pytz
import dateparser


SCOPES = ['https://www.googleapis.com/auth/calendar']

DAYS = ['понедельник', 'вторник', 'среда',
        'четверг', 'пятница', 'суббота', 'воскресенье']
MONTHS = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь',
          'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь']

calendar_id = 'br7erpcvjjvrgu05iiibci7ua0@group.calendar.google.com'


def authenticate_google():
    # -------------------------------------------------------- #
    # аутентификация.                                          #
    # для работы на сервере необходим файл 'credentials.json'. #
    # выполняется автоматически один раз при первом запуске    #
    # -------------------------------------------------------- #
    creds = None

    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)

    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)

        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('calendar', 'v3', credentials=creds)
    return service


def get_events(service, day):
    utc = pytz.UTC
    time_start = datetime.datetime.combine(
        day, datetime.datetime.min.time()).astimezone(utc).isoformat()
    time_end = datetime.datetime.combine(
        day, datetime.datetime.max.time()).astimezone(utc).isoformat()

    events_result = service.events().list(
        calendarId=calendar_id,
        timeMin=time_start,
        timeMax=time_end,
        singleEvents=True,
        orderBy='startTime'
    ).execute()
    
    events = events_result.get('items', [])

    if not events:
        return 'Предстоящие события не найдены.'
    for event in events:
        start = event['start'].get('dateTime', event['start'].get('date'))
        return (start, event['summary'])


def create_event(service, summary='Встреча', description='-',
                 dateTimeStart=datetime.datetime.now(), hours=2):

    # ----------------------------------------------------- #
    # создание ивента по полученным данным от пользователя, #
    # по умолчанию создает событие Встреча в момент вызова  #
    # ----------------------------------------------------- #

    timeZone = 'Europe/Moscow'
    dateTimeEnd = dateTimeStart + datetime.timedelta(hours=hours)

    event = {
        'summary': summary,
        'description': description,
        'start': {
            'dateTime': dateTimeStart.strftime('%Y-%m-%dT%H:%M:%S'),
            'timeZone': timeZone,
        },
        'end': {
            'dateTime': dateTimeEnd.strftime('%Y-%m-%dT%H:%M:%S'),
            'timeZone': timeZone,
        },
        'reminders': {
            'useDefault': False,
            'overrides': [
                {'method': 'email', 'minutes': 24 * 60},
                {'method': 'popup', 'minutes': 10}
            ],
        },
    }

    event = service.events().insert(
        calendarId='br7erpcvjjvrgu05iiibci7ua0@group.calendar.google.com',
        body=event
    ).execute()

    print('Event created: {0}'.format(event.get('htmlLink')))


def get_date(text):

    # ----------------------------------------------------------------- #
    # получает инфмормацию о дате из сообщения пользователя             #
    # и возвращает её в формате datetime                                #
    # пока не воспринимает числительные типа "первого" "второго" и т.д. #
    # ----------------------------------------------------------------- #

    text = text.lower()
    today = datetime.date.today()
    tomorrow = datetime.date.today() + datetime.timedelta(days=1)
    day_after_tomorrow = datetime.date.today() + datetime.timedelta(days=2)

    day = -1
    day_of_week = -1
    month = -1
    year = today.year

    for word in text.split():
        if fuzz.ratio(word, 'сегодня') >= 70:
            return today
        elif fuzz.ratio(word, 'послезавтра') >= 70:
            return day_after_tomorrow
        elif fuzz.ratio(word, 'завтра') >= 70:
            return tomorrow

    for word in text.split():
        if word.isdigit():
            day = int(word)
        for month_ in MONTHS:
            if fuzz.ratio(word, month_) >= 70:
                month = MONTHS.index(month_) + 1
        for day_ in DAYS:
            if fuzz.ratio(word, day_) >= 70:
                day_of_week = DAYS.index(day_)

    # проверяет, принадлежит ли данный месяц к следующему году
    if month < today.month and not month == -1:
        year = year + 1

    # проверяет, принаждлежит ли данный день к следующему году
    if day < today.day and not month == -1 and not day == -1:
        month = month + 1

    # получает информацию о дате, о которой говорит пользователь
    # если он не уточняет её точно
    if month == -1 and day == -1 and not day_of_week == -1:
        current_day_of_week = today.weekday()  # from 0 to 6
        difference = day_of_week - current_day_of_week

        if difference < 0:
            difference = difference + 7

        return today + datetime.timedelta(difference)


def get_event_data(text):
    pass
    # обрабатывает сообщение для получения и структуризирования данных для создания ивента


def main():
    service = authenticate_google()
    d = dateparser.parse(input('Date: '))
    print(d)
    print(get_events(service, d))
    
    create_event(
        service=service,
        summary=input('Enter a summary: '),
        description=input('Enter the description: '),
        dateTimeStart=d if d is not None else datetime.datetime(2020, 3, 19, 15, 30, 0),
        hours=2
    )


if __name__ == '__main__':
    main()
