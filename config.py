from dotenv import load_dotenv
import os

basedir = os.path.abspath(os.path.dirname(__file__))

dotenv_path = os.path.join(basedir, '.env')
load_dotenv(dotenv_path=dotenv_path)

VK_GROUP_TOKEN = os.environ.get('VK_GROUP_TOKEN', '')
APP_ACCESS_TOKEN = os.environ.get('ACCESS_TOKEN') or ''
APP_SERICE_KEY = os.environ.get('APP_SERVICE_KEY') or ''
APP_ID = os.environ.get('APP_ID', None)

VK_LOGIN = os.environ.get('VK_LOGIN')
VK_PASSWORD = os.environ.get('VK_PASSWORD')

PROJECTS_INFO_PATH = os.path.join(basedir, 'projects.json')

# Just for debug, as there's really no way to see if the env. vars have been loaded or not
if VK_PASSWORD and VK_LOGIN and VK_GROUP_TOKEN:
    print('[DOTENV] VK ENVIRONMENT VARIABLES LOADED')


class Config:
    FLASK_ENV = os.environ.get('FLASK_ENV') or 'dev'
    FLASK_DEBUG = os.environ.get('FLASK_DEBUG') or True
    SECRET_KEY = os.environ.get('SECRET_KEY', b',asdml9a8h12n')
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    SQLALCHEMY_POOL_RECYCLE = 299
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # Same as above, debug
    if SQLALCHEMY_DATABASE_URI:
        print('[DOTENV] SQLALCHEMY_DATABASE_URI LOADED')

